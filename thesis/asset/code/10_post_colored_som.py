
# coding: utf-8

# # 10 - Post colored SOM

# In[24]:

import os
import math
import pickle
import pandas as pd
import numpy as np
from som import som
import scipy.constants as const
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
get_ipython().magic(u'matplotlib inline')
from IPython.display import display, display_markdown
from random import shuffle
from datetime import date
import kohonen
from lib.Utils import computeUMatrix, constructSamplesForNeurons, ExponentialTimeseries as ET
from mpl_toolkits.axes_grid1 import make_axes_locatable


# ## Functions

# In[32]:

def constructSamplesForNeurons(kmap, matrix, index):
    samplesDict = dict()
    for i, row in enumerate(matrix):
        j = kmap.winner(row)
        x,y = kmap.flat_to_coords(j)
        key = str(x) + "," + str(y)
        if key not in samplesDict.keys():
            samplesDict[key] = list()
        samplesDict[key].append(index[i])
    return samplesDict


def dictToDf(sampleDict, som):
    dimx = som.params.get('x')
    dimy = som.params.get('y')
    matrix = np.ndarray(shape=(dimx, dimy))
    matrix.fill(0)
    df = pd.DataFrame(matrix, dtype=list)
    for key, value in sampleDict.iteritems():
        nx, ny = key.split(",")
        nx = int(nx)
        ny = int(ny)
        df.loc[nx, ny] = value
    return df


def variablePlane(df, patients, var):
    # Iterate all the neurons
    for x in df.columns:
        for y in df.index:
            cell = df.loc[y, x]
            if cell != 0: # invalid cells are filled with 0 at matrix init
                # Iterate the patients of this neuron 
                for idx_c, p in enumerate(cell):
                    for idx_p, v in patients[var].iteritems():
                        if idx_p == p:
                            cell[idx_c] = v
    for x in df.columns:
        for y in df.index:
            cell = df.loc[y, x]
            if cell != 0: # invalid cells are filled with 0 at matrix init
                # Iterate the patients of this neuron
                df.loc[y, x] = np.array(cell).mean()
    
    df = pd.DataFrame(df, dtype=float)
    return df


def planeComparison(var, df1, df2, params):
    # standard dataset
    df_wo = df1.copy()
    # augmented dataset
    df_w = df1.copy()
    df_w[var] = df2[var]
    df_w = df_w.fillna(df_w.mean())
    # Calculate the SOMs
    soms = []
    for i, val in enumerate([df_wo, df_w]):
        params['dimension'] = len(list(val.columns))
        compSom = som.SOM(val.as_matrix(), list(val.columns), params)
        somDict = {'som': compSom,
                   'df' : val}
        soms.append(somDict)
    dfs = [df1, df2]
    soms[0]['title'] = 'steroids only'
    soms[1]['title'] = 'steroids with '+var
    return {'soms'   : soms,
            'dfs'    : dfs,
            'var'    : var}


def display(comp):
    fig = plt.figure(figsize=(20,20))
    gs = gridspec.GridSpec(1, 2)
    df_vars = []
    for i, val in enumerate(comp['soms']):
        # Make the variable component planes
        tmp = constructSamplesForNeurons(val['som'].kmap, val['som'].matrix, val['df'].index)
        df_var = dictToDf(tmp, val['som'])
        df_var = variablePlane(df_var, comp['dfs'][1], comp['var'])
        df_vars.append(df_var)
        # Make the plot grid
        plt.subplot(gs[i])
        plt.title(val['title'], size = 20, y=1.08)  
        ax = plt.gca()
        im = ax.imshow(df_var.as_matrix(), interpolation="nearest", vmin=0, vmax=df_var.max().max())
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="3%", pad=0.05)
        plt.colorbar(im, cax=cax)
        
    plt.tight_layout(pad=0, w_pad=0.5, h_pad=0.5)
    return df_vars


def getAge(dob):
    #year, month, day = dob.split("-") # To use if dob is a string
    today = date.today()
    return today.year - int(dob.year) - ((today.month, today.day) < (int(dob.month), int(dob.day)))


# ## Data import and preparation

# ### Steroids

# In[26]:

steroids = pickle.load(open("output/01_steroids_cleaned.pickle", "rb"))

attributes = [
        'cr_saad3a17b_d', 'cr_ll_oxo_etio_d', 'cr_llb_oh_andro_d', 'cr_llb_oh_etio_d', 'cr_dha_d',
        'cr_s_ad_17b_d', 'cr_l6a_oh_dha_d', 'cr_s_at_d', 'cr_s_pt_d', 'cr_testosterone_d', 'cr_sa_dihydrotest_d',
        'cr_estriol_d', 'cr_l7b_estradiol_d', 'cr_pd_d', 'cr_pt_d', 'cr_pt_one_d',
        'cr_ths_d', 'cr_thaldo_d', 'cr_thdoc_d', 'cr_tha_d', 'cr_thb_d', 'cr_sa_thb_d', 'cr_l8_oh_tha_d',
        'cr_cortisone_d', 'cr_the_d', 'cr_a_cortolone_d', 'cr_b_cortolone_d', 'cr_zoa_dhe_d', 'cr_zob_dhe_d',
        'cr_cortisol_d', 'cr_a_cortol_d', 'cr_b_cortol_d', 'cr_zoa_dhf_d', 'cr_sb_oh_f_d', 'cr_l8_oh_f_d'
]

attributes = [v.lower() for v in attributes]
print len(attributes)


# ### Patients

# In[27]:

file = "data/data_heig.dta"
data = pd.read_stata(file)
data = data.iloc[:1129]
patients = data[['dob', 'sex', 'bmi','dbp_24_c_m','sbp_24_c_m']]


# In[28]:

patients.shape


# In[29]:

# Keep only the valid patients
valid_rows = pickle.load(open("output/01_patients_to_keep.pickle", "rb"))
patients = patients.iloc[valid_rows]


# In[30]:

patients.shape


# #### Calculate age from date of birth

# In[33]:

patients['age'] = patients['dob'].apply(getAge)
patients.head()


# #### Drop unneeded columns

# In[34]:

patients = patients.drop(['dob'], 1)
patients.head()


# ### Serialized SOM

# In[35]:

somSteroids = pickle.load(open("output/09_steroid_calibrated_som.pickle", "rb"))


# ----------------
# 
# ## Build the colored component planes

# In[44]:

variables = list(patients.columns)
# Male = 1; Female = 2

fig = plt.figure(figsize=(20,20))
gs = gridspec.GridSpec(3, 2)

for i, var in enumerate(variables):
    
    tmp = constructSamplesForNeurons(somSteroids.kmap, somSteroids.matrix, steroids.index)
    df = dictToDf(tmp, somSteroids)
    df2 = variablePlane(df, patients, var)
    
    plt.subplot(gs[i])
    plt.title(variables[i], size = 20, y=1.08)  
    ax = plt.gca()
    im = ax.imshow(df2.as_matrix(), interpolation="nearest", vmin=0, vmax=df2.max().max())
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="3%", pad=0.05)
    plt.colorbar(im, cax=cax)
    plt.savefig("img/10_all.png", bbox_inches = 'tight', dpi=300)
    
plt.tight_layout(pad=0, w_pad=0.5, h_pad=0.5)


# In[49]:

variables = list(patients.columns)
# Male = 1; Female = 2

fig = plt.figure(figsize=(20,20))
gs = gridspec.GridSpec(3, 2)

for i, var in enumerate(variables):
    
    tmp = constructSamplesForNeurons(somSteroids.kmap, somSteroids.matrix, steroids.index)
    df = dictToDf(tmp, somSteroids)
    df2 = variablePlane(df, patients, var)
    
    fig1 = plt.figure(figsize=(10,10))
    plt.title(variables[i], size = 20, y=1.08)
    ax = plt.gca()
    im = ax.imshow(df2.as_matrix(), interpolation="nearest", vmin=0, vmax=df2.max().max())
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="3%", pad=0.05)
    plt.colorbar(im, cax=cax)
    _ = plt.savefig("img/10_"+var+".png", bbox_inches = 'tight', dpi=300)
    
plt.tight_layout(pad=0, w_pad=0.5, h_pad=0.5)


# ## Comparison

# Comparaison entre variable planes fait à partir d'une SOM:
# - avec uniquement des stéroïdes
# - avec les stéroïdes et une variable choisie 
# 
# possible values: 'age', 'sex', 'bmi','dbp_24_c_m','sbp_24_c_m'

# ### Preparation

# In[50]:

variables = list(patients.columns)
params = {'n_iter': 30, 'learning_rate': (0.2, 0.0001), 'neighborhood_size': (33/2, 1), 'y': 33, 'x': 20, 'dimension': 35}


# In[51]:

params = {
            'n_iter' : 30, 
            'x' : 20,
            'y' : int(np.ceil(const.golden*20)),
            'dimension' : None,
            'learning_rate' : (0.012, 0.001),
            'neighborhood_size' : (int(np.ceil(1./2*int(np.ceil(const.golden*20)))), 1) 
        }
params


# ### Functions

# ### Age

# In[52]:

age = planeComparison('age', steroids, patients, params);


# In[53]:

display(age);
_ = plt.savefig("img/10_age_comparison.png", bbox_inches = 'tight', dpi=300)


# In[54]:

filename = "output/10_age_comparison.pickle"  
with open(filename, "w+b") as f:
    pickle.dump(age, f)


# ### Sex
# 
# - Male = 1
# - Female = 2

# In[55]:

sex = planeComparison('sex', steroids, patients, params);


# In[56]:

display(sex);
_ = plt.savefig("img/10_sex_comparison.png", bbox_inches = 'tight', dpi=300)


# In[57]:

filename = "output/10_sex_comparison.pickle"  
with open(filename, "w+b") as f:
    pickle.dump(sex, f)


# ### BMI

# In[58]:

bmi = planeComparison('bmi', steroids, patients, params);


# In[59]:

display(bmi);
_ = plt.savefig("img/10_bmi_comparison.png", bbox_inches = 'tight', dpi=300)


# In[60]:

filename = "output/10_bmi_comparison.pickle"  
with open(filename, "w+b") as f:
    pickle.dump(bmi, f)


# ### dbp_24_c_m
# 
# mean of dbp during 24h without outliers

# In[61]:

dbp_24_c_m = planeComparison('dbp_24_c_m', steroids, patients, params);


# In[62]:

display(dbp_24_c_m);
_ = plt.savefig("img/10_dbp_24_c_m_comparison.png", bbox_inches = 'tight', dpi=300)


# In[63]:

filename = "output/10_dbp_24_c_m_comparison.pickle"  
with open(filename, "w+b") as f:
    pickle.dump(dbp_24_c_m, f)


# ### sbp_24_c_m
# 
# mean of sbp during 24h without outliers

# In[64]:

sbp_24_c_m = planeComparison('sbp_24_c_m', steroids, patients, params);


# In[66]:

filename = "output/10_sbp_24_c_m_comparison.pickle"  
with open(filename, "w+b") as f:
    pickle.dump(sbp_24_c_m, f)


# In[68]:

display(sbp_24_c_m);
_ = plt.savefig("img/10_sbp_24_c_m_comparison.png", bbox_inches = 'tight', dpi=300)


# In[ ]:



