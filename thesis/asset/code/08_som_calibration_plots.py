
# coding: utf-8

# # 08 - SOM Calibration - Plots

# In[2]:

import os
import math
import pickle
import pandas as pd
import numpy as np
from som import som
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
get_ipython().magic(u'matplotlib inline')
from IPython.display import display, display_markdown
from random import shuffle


# # Data import

# In[108]:

# Set the run type
runType = "test" # [test, lowRes, hiRes]

# Load the data
results = pickle.load(open('output/'+runType+'/160526_1126_result_steroids_test.pickle', "rb"))
summaries = pickle.load(open('output/'+runType+'/160526_1126_summary_steroids_test.pickle', "rb"))


# In[109]:

# X = dimension (xdim)
# Y = Nb of iterations (n_iter)
results.head()


# In[110]:

results.shape


# In[111]:

# X = dimension (xdim)
# Y = Nb of iterations (n_iter)
summaries.head()


# In[112]:

# Set the first SOM size summary as exemple
h0 = summaries.index[0]
w0 = summaries.columns[0]
initial = summaries[w0][h0]


# ## Plot de l'erreur médiane

# Erreur médiane sur les n exécutions faites durant le grid run

# In[113]:

fig, ax = plt.subplots(figsize = (10,5))
plt.title("Median error")
plt.xlabel('Iterations')
plt.ylabel('Error')
ax = plt.plot(initial["median_error"], '-r')
ax = plt.fill_between(initial.index, initial["lower_whisker_error"], initial["upper_whisker_error"], facecolor='red', alpha=0.3)
_ = plt.savefig("img/08_median_error_steroids_"+runType+".png", bbox_inches = 'tight', dpi=300)


# ## Vue globale de l'erreur médiane

# In[114]:

fig2 = plt.figure(figsize=(40,40))

h = len(summaries.index) # Y = Nb of iterations (n_iter)
w = len(summaries.columns) # X = dimension (xdim)
h0 = summaries.index[0]
w0 = summaries.columns[0]
hGap = summaries.index[1]-h0
wGap = summaries.columns[1]-w0
gs = gridspec.GridSpec(h, w)

for i in range(h):
    for j in range(w):
        ax = plt.subplot(gs[j+(i*w)])
        n_iter = h0+(i*hGap) # Y = Nb of iterations (n_iter)
        xdim = w0+(j*wGap) # X = dimension (xdim)
        plt.title("xdim:"+str(xdim)+" n_iter:"+str(n_iter))  
        plt.xlabel('Iterations')
        ax.set_xlim([0, summaries.index[-1]])
        ax.set_ylim([0, np.around(summaries[w0][h0]["median_error"].max())])
        plt.ylabel('Error')
        plt.plot(summaries[xdim][n_iter]["median_error"], '-r')

plt.tight_layout(pad=0, w_pad=0.5, h_pad=1.2)
_ = plt.savefig("img/08_global_median_error_steroids_"+runType+".png", bbox_inches = 'tight', dpi=300)


# ## Dérivée de l'erreur médiane

# ### Plot de la dérivée de l'erreur et de l'erreur

# In[115]:

plt.figure(figsize=(10, 5))
plt.plot(initial["median_error"], 'b--')
plt.plot(initial["slope"], 'r')
_ = plt.savefig("img/08_error_derivative_steroids_"+runType+".png", bbox_inches = 'tight', dpi=300)


# ### Plot de la dérivée de l'erreur

# In[116]:

plt.figure(figsize=(10, 5))
plt.plot(initial["slope"], 'r')
_ = plt.savefig("img/08_derivative_steroids_"+runType+".png", bbox_inches = 'tight', dpi=300)


# # Optimisation avec l'erreur

# In[117]:

optimisations = pd.DataFrame()

for size in summaries:
    iterations = []
    for run in summaries[size]:
        iterations.append(run["median_error"].iloc[run.index[-1]])
    optimisations[size] = iterations
optimisations.index = summaries.index


# In[118]:

optimisations


# In[119]:

fig = plt.figure()
ax = fig.gca(projection='3d')

X = optimisations.columns
Y = optimisations.index

X, Y = np.meshgrid(X, Y)
Z = optimisations

surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet,
                       linewidth=0, antialiased=True)

ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
# X = dimension (xdim)
# Y = Nb of iterations (n_iter)
fig.colorbar(surf, shrink=0.5, aspect=5)
ax.set_xlabel('xdim')
ax.set_ylabel('n_iter')
ax.set_zlabel('error')

#for angle in range(0, 360):
ax.view_init(30, 60)

plt.tight_layout()
_ = plt.savefig("img/08_grid_error_steroids_"+runType+".png", bbox_inches = 'tight', dpi=300)
plt.show()


# In[120]:

plt.matshow(optimisations)

