
# coding: utf-8

# # 04 - SOM Package

# In[2]:

import os
import math
import pickle
import pandas as pd
import numpy as np
from som import som
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
get_ipython().magic(u'matplotlib inline')
from IPython.display import display, display_markdown
from random import shuffle


# ## Données de test

# In[3]:

animals = ["dove", "hen", "duck", "goose", "owl", "hawk", "eagle", "fox",
           "dog", "wolf", "cat", "tiger", "lion", "horse", "zebra", "cow"]

attributes = ["small", "medium", "big", "2legs", "4legs", "hair", "hooves",
              "mane", "feathers", "hunt", "run", "fly", "swim"] 

matrix_animal = np.array([
        [1,1,1,1,1,1,0,0,0,0,1,0,0,0,0,0],
        [0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1],
        [1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1],
        [0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1],
        [0,0,0,1,0,0,0,0,0,1,0,0,1,1,1,0],
        [1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,1,1,1,1,0,1,1,1,1,0,0,0],
        [0,0,0,0,0,0,0,0,1,1,0,1,1,1,1,0],
        [1,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0],
        [0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0]])

matrix_animal = np.transpose(matrix_animal)


# ## Calcul d'une SOM

# In[4]:

params = {
            'n_iter' : 20, 
            'x' : 20,
            'y' : 20,
            'dimension' : len(attributes),
            'learning_rate' : (0.2, 0.05),
            'neighborhood_size' : (2./3*20, 1) 
        }


# In[5]:

somAnimals = som.SOM(matrix_animal, attributes, params)


# ## U-Matrix

# In[6]:

somAnimals.getUMatrix();
_ = plt.savefig("img/04_som_toy.png", bbox_inches = 'tight', dpi=300)


# ## Component planes

# In[39]:

somAnimals.getComponentPlanes()
_ = plt.savefig("img/04_planes_toy.png", bbox_inches = 'tight', dpi=300)


# ## Plot de l'erreur

# In[40]:

somAnimals.getErrorPlot()
_ = plt.savefig("img/04_error_toy.png", bbox_inches = 'tight', dpi=300)


# In[ ]:



