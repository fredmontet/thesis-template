
# coding: utf-8

# # 02 - Steroid SOM

# In[2]:

import os
import pickle
import pandas as pd
import numpy as np
import skimage
import skimage.io
import pylab as pl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.gridspec as gridspec
get_ipython().magic(u'matplotlib inline')
import kohonen
import math
from random import shuffle
from lib.Utils import computeUMatrix, constructSamplesForNeurons, ExponentialTimeseries as ET
from IPython.display import display, display_markdown


# ## Import the data

# In[5]:

steroids = pickle.load(open("output/01_steroids_cleaned.pickle", "rb"))


# ## Select the metabolites

# The row above 1130 (1-based and including header) contain individuals which seem to be parents of other included in the study but who were not included themselves.

# In[6]:

variables = [
        'cr_saad3a17b_d', 'cr_ll_oxo_etio_d', 'cr_llb_oh_andro_d', 'cr_llb_oh_etio_d', 'cr_dha_d',
        'cr_s_ad_17b_d', 'cr_l6a_oh_dha_d', 'cr_s_at_d', 'cr_s_pt_d', 'cr_testosterone_d', 'cr_sa_dihydrotest_d',
        'cr_estriol_d', 'cr_l7b_estradiol_d', 'cr_pd_d', 'cr_pt_d', 'cr_pt_one_d',
        'cr_ths_d', 'cr_thaldo_d', 'cr_thdoc_d', 'cr_tha_d', 'cr_thb_d', 'cr_sa_thb_d', 'cr_l8_oh_tha_d',
        'cr_cortisone_d', 'cr_the_d', 'cr_a_cortolone_d', 'cr_b_cortolone_d', 'cr_zoa_dhe_d', 'cr_zob_dhe_d',
        'cr_cortisol_d', 'cr_a_cortol_d', 'cr_b_cortol_d', 'cr_zoa_dhf_d', 'cr_sb_oh_f_d', 'cr_l8_oh_f_d'
]

variables = [v.lower() for v in variables]
print len(variables)


# ## data overview

# In[7]:

steroids.iloc[:10,:]


# In[8]:

matrix = steroids.as_matrix()


# ## SOM function

# In[9]:

def makeSom(matrix, variables, cp, label, n_iter, x, y, side, path, name):
    params = kohonen.Parameters(dimension = side, 
                                shape = (x, y),
                                metric = kohonen.cosine_metric
                                )
    kmap = kohonen.Map(params)

    ## Learns the dataset n_iter times in a randomly defined order
    ## The learning rate decreases exponentially from 1 to 0.2
    kmap._learning_rate = ET(1, 0.2, n_iter*matrix.shape[0])
    kmap._neighborhood_size = ET(4./3*side, 1, n_iter*matrix.shape[0])
    for i in range(0,n_iter):
        order = np.arange(0, matrix.shape[0], 1)
        shuffle(order)
        for j in order:
            kmap.learn(matrix[j])

    ## Computes distance between neurons and creates a U-Matrix (of 2*len(animals) x len(animals)*4)
    u_matrix = computeUMatrix(kmap)

    ## Creates a colormap for the U-Matrix
    colors = cm.Spectral_r
    colors.set_bad('w',1.)

    ## Gets winning neuron for each sample (1-nn)
    steroidDict = constructSamplesForNeurons(kmap, matrix)

    ## Prints U-Matrix
    fig1 = plt.figure(figsize=(10,10))
    plt.imshow(u_matrix, cmap=colors, interpolation="nearest")
    plt.colorbar(shrink=0.7)
    plt.axis('off')
    
    ## Prints steroids on map
    if(label):
        for neuron in steroidDict:
            neuronx, neurony = neuron.split(",")
            neuronx = int(neuronx) * 2
            neurony = int(neurony) * 2
            pl.scatter(neurony, neuronx)
            steroidNames = ""
            for steroidID in steroidDict[neuron]:
                steroidNames += variables[steroidID] + "\n"
            pl.annotate(steroidNames, (neurony, neuronx), size=15)
    
    ## Save the U-Matrix
    pl.savefig(path+"/02_som_"+name+"_"+str(x)+"x"+str(y)+"_"+str(n_iter)+"_iter.png", bbox_inches = 'tight', dpi=300)
    
    ## Prints component plans
    if(cp):
        fig2 = plt.figure(figsize=(40,40))
        gs = gridspec.GridSpec(8, 5)

        for i in range(len(variables)):
            plt.subplot(gs[i])
            plt.title(variables[i], size = 15, y=1.08)  
            plt.imshow(kmap.neurons[:,:,i], interpolation="nearest", vmin=0, vmax=kmap.neurons.max())
            plt.colorbar(shrink=0.7)
            plt.axis('off')

        plt.tight_layout(pad=0, w_pad=0.5, h_pad=1.2)
        plt.savefig(path+"/02_component_planes_"+name+"_"+str(x)+"x"+str(y)+"_"+str(n_iter)+"_iter.png", dpi=300)


# ## SOM on steroids

# In[11]:

matrix = steroids.as_matrix()
matrix = np.transpose(matrix)
#makeSom(matrix, variables, cp, label, n_iter,  x,  y, side, name)
makeSom(matrix, variables, False, True, 40, 40, 40, len(variables), "./img", "patient")


# Cette SOM n'est pas ce qu'on souhaite. On y voit les groupes de stéroïdes qui sont souvent ensembles.

# ## SOM on patients

# Sur cette SOM on distingue quelques groupes...

# In[10]:

matrix = steroids.as_matrix()
#makeSom(matrix, variables, cp, label, n_iter,  x,  y, side, path, name)
makeSom(matrix, variables, True, True, 40, 40, 40, len(variables), "./img", "steroids")


# ## Find the optimal parameters

# Première tentative de grid search naïve

# In[ ]:

for x in range(10, 200, 10):
    for n_iter in range(10, 100, 10):
        #makeSom(matrix, variables, cp   , label, n_iter,  x,  y, side, path, name):
        makeSom (matrix, variables, False, False, n_iter,  x,  x, len(variables), "./img/02_test/", "patient")    

