
# coding: utf-8

# # 05 - SOM Calibration Test - Grid run

# In[2]:

import os
import math
import pickle
import pandas as pd
import numpy as np
from som import som
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
get_ipython().magic(u'matplotlib inline')
from IPython.display import display, display_markdown
from random import shuffle


# # Functions

# In[ ]:

def slope(x1, x2, y1, y2):
    return (y2-y1)/(x2-x1)

def angle(x1, x2, y1, y2):
    return math.degrees(math.atan((x2-x1)/(y2-y1)))
    
def slopeLine(x,y):
    m = []
    for i in range(len(x)):
        if i == x[0]:
            m.append(np.nan)
        else:
            m.append(slope(x[i-1],x[i],y[i-1],y[i]))
    return m

def angleLine(x,y):
    m = []
    for i in range(len(x)):
        if i == x[0]:
            m.append(np.nan)
        else:
            m.append(angle(x[i-1],x[i],y[i-1],y[i]))
    return m


# ## Données de test

# In[4]:

# Data import

animals = ["dove", "hen", "duck", "goose", "owl", "hawk", "eagle", "fox",
           "dog", "wolf", "cat", "tiger", "lion", "horse", "zebra", "cow"]

attributes = ["small", "medium", "big", "2legs", "4legs", "hair", "hooves",
              "mane", "feathers", "hunt", "run", "fly", "swim"] 

matrix_animal = np.array([
        [1,1,1,1,1,1,0,0,0,0,1,0,0,0,0,0],
        [0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1],
        [1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1],
        [0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1],
        [0,0,0,1,0,0,0,0,0,1,0,0,1,1,1,0],
        [1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,1,1,1,1,0,1,1,1,1,0,0,0],
        [0,0,0,0,0,0,0,0,1,1,0,1,1,1,1,0],
        [1,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0],
        [0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0]])

matrix_animal = np.transpose(matrix_animal)


# ## Exécution du batch de SOM

# In[6]:

## Execution parameters
xdim_range = range(10, 110, 10)
n_iter_range = range(10, 110, 10)
nb_exec = range(5)


# In[34]:

## Execution loop
results = pd.DataFrame()

for xdim in xdim_range:
    iterations = []
    for n_iter in n_iter_range:
        params = {
            'n_iter' : n_iter, 
            'x' : xdim,
            'y' : xdim,
            'dimension' : len(attributes),
            'learning_rate' : (0.2, 0.05),
            'neighborhood_size' : (2./3*20, 1) 
        }
        print params
        errors = pd.DataFrame()
        for i in nb_exec:
            somAnimals = som.SOM(matrix_animal, attributes, params)
            errors[i] = somAnimals.error
        iterations.append(errors)
    results[xdim] = iterations
results.index = n_iter_range


# ## Résultats

# In[38]:

# X = dimension (xdim)
# Y = Nb of iterations (n_iter)
results


# In[39]:

# 20x20 with 30 iterations
# results[xdim][n_iter]
# X = Execution number
# Y = Iteration number
results[20][30]


# In[40]:

results.to_pickle('output/05_results_test_'+xdim+'_'+n_iter+'.pickle')


# ## Résumés des résultats

# In[41]:

summaries = pd.DataFrame()

for size in results:
    iterations = []
    for run in results[size]:
        summary = pd.DataFrame()
        run = run.transpose()

        upper_quartile = run.quantile(.75)
        lower_quartile = run.quantile(.25)
        iqr = upper_quartile - lower_quartile
        upper_whisker = run[run<=upper_quartile+1.5*iqr].max()
        lower_whisker = run[run>=lower_quartile-1.5*iqr].min()

        summary["lower_whisker_error"] = lower_whisker
        summary["upper_whisker_error"] = upper_whisker
        summary["median_error"] = run.median()
        summary["slope"] = slopeLine(summary.index, summary["median_error"])
        summary["angle"] = angleLine(summary.index, summary["slope"])

        iterations.append(summary)
    summaries[size] = iterations
summaries.index = n_iter_range
    


# In[42]:

# The number of different SOM sizes
len(summaries)


# In[43]:

# X = dimension (xdim)
# Y = Nb of iterations (n_iter)
summaries


# In[44]:

summaries[20][20]


# In[45]:

summaries.to_pickle('output/05_summaries_test_'+xdim+'_'+n_iter+'.pickle')

