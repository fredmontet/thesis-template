
# coding: utf-8

# # 03 - Steroids global correlations

# In[1]:

import os
import pickle
import pandas as pd
import numpy as np
import skimage
import skimage.io
import pylab as pl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.gridspec as gridspec
get_ipython().magic(u'matplotlib inline')
import math
from random import shuffle
from IPython.display import display, display_markdown
from pandas.tools.plotting import scatter_matrix
from pandas.tools.plotting import parallel_coordinates
from pandas.tools.plotting import _subplots, _get_marker_compat, _set_ticks_props
import pandas.core.common as com
from pandas.compat import range, lrange, lmap, map, zip, string_types
from scipy.stats import pearsonr
from mpl_toolkits.axes_grid1 import make_axes_locatable


# In[2]:

# Parameters
img_path = "./img/" # The path where the graphics will be saved
tex_path = "./table/" # The path to save the LaTeX tables


# ## Functions

# In[3]:

def save_latex_table(table, name):
    file_path = tex_path + "table_"+ name +".tex"
    with open(file_path, "w") as text_file:
        text_file.write(table.to_latex(index=False))
        print "-> " + file_path + " saved!"

def custom_scatter_matrix(frame, alpha=0.5, figsize=None, ax=None, grid=False,
                   diagonal='hist', marker='.', density_kwds=None,
                   hist_kwds=None, range_padding=0.05, p_min=0.05, **kwds):
    """
    Draw a matrix of scatter plots.
    Parameters
    ----------
    frame : DataFrame
    alpha : float, optional
        amount of transparency applied
    figsize : (float,float), optional
        a tuple (width, height) in inches
    ax : Matplotlib axis object, optional
    grid : bool, optional
        setting this to True will show the grid
    diagonal : {'hist', 'kde'}
        pick between 'kde' and 'hist' for
        either Kernel Density Estimation or Histogram
        plot in the diagonal
    marker : str, optional
        Matplotlib marker type, default '.'
    hist_kwds : other plotting keyword arguments
        To be passed to hist function
    density_kwds : other plotting keyword arguments
        To be passed to kernel density estimate plot
    range_padding : float, optional
        relative extension of axis range in x and y
        with respect to (x_max - x_min) or (y_max - y_min),
        default 0.05
    kwds : other plotting keyword arguments
        To be passed to scatter function
    Examples
    --------
    >>> df = DataFrame(np.random.randn(1000, 4), columns=['A','B','C','D'])
    >>> scatter_matrix(df, alpha=0.2)
    """

    df = frame._get_numeric_data()
    cl = []
    cm = []
    pm = []
    n = df.columns.size
    naxes = n * n
    fig, axes = _subplots(naxes=naxes, figsize=figsize, ax=ax,
                          squeeze=False)

    # no gaps between subplots
    fig.subplots_adjust(wspace=0, hspace=0)

    mask = com.notnull(df)

    marker = _get_marker_compat(marker)

    hist_kwds = hist_kwds or {}
    density_kwds = density_kwds or {}

    # workaround because `c='b'` is hardcoded in matplotlibs scatter method
    kwds.setdefault('c', plt.rcParams['patch.facecolor'])

    boundaries_list = []
    for a in df.columns:
        values = df[a].values[mask[a].values]
        rmin_, rmax_ = np.min(values), np.max(values)
        rdelta_ext = (rmax_ - rmin_) * range_padding / 2.
        boundaries_list.append((rmin_ - rdelta_ext, rmax_ + rdelta_ext))

    for i, a in zip(lrange(n), df.columns):
        tmp_cm = [] # for pearson coef
        tmp_pm = [] # for p-value
        
        for j, b in zip(lrange(n), df.columns):
            ax = axes[i, j]

            if i == j:
                values = df[a].values[mask[a].values]

                # Deal with the diagonal by drawing a histogram there.
                if diagonal == 'hist':
                    ax.hist(values, **hist_kwds)

                elif diagonal in ('kde', 'density'):
                    from scipy.stats import gaussian_kde
                    y = values
                    gkde = gaussian_kde(y)
                    ind = np.linspace(y.min(), y.max(), 1000)
                    ax.plot(ind, gkde.evaluate(ind), **density_kwds)
                
                tmp_cm.append(np.nan) # For the pearson coef matrix
                tmp_pm.append(np.nan) # For the p-value matrix
                ax.set_xlim(boundaries_list[i])

            else:
                common = (mask[a] & mask[b]).values

                ax.scatter(df[b][common], df[a][common],
                           marker=marker, alpha=alpha, **kwds)
                
                pCorr, pVal = pearsonr(df[b][common], df[a][common])
                tmp_cl = [a, b, pCorr, pVal]                
                
                tmp_cm.append(pCorr) # For the pearson coef matrix
                tmp_pm.append(pVal) # For the p-value matrix
                cl.append(tmp_cl) # To create the correlation df
                
                ax.patch.set_alpha(0.4)

                if pVal <= p_min:
                    ax.set_axis_bgcolor('green')
                else:
                    ax.set_axis_bgcolor('red')

                ax.set_xlim(boundaries_list[j])
                ax.set_ylim(boundaries_list[i])
            
            ax.set_xlabel(b)
            ax.set_ylabel(a)

            if j != 0:
                ax.yaxis.set_visible(False)
            if i != n - 1:
                ax.xaxis.set_visible(False)
        
        cm.append(tmp_cm)
        pm.append(tmp_pm)

    if len(df.columns) > 1:
        lim1 = boundaries_list[0]
        locs = axes[0][1].yaxis.get_majorticklocs()
        locs = locs[(lim1[0] <= locs) & (locs <= lim1[1])]
        adj = (locs - lim1[0]) / (lim1[1] - lim1[0])

        lim0 = axes[0][0].get_ylim()
        adj = adj * (lim0[1] - lim0[0]) + lim0[0]
        axes[0][0].yaxis.set_ticks(adj)

        if np.all(locs == locs.astype(int)):
            # if all ticks are int
            locs = locs.astype(int)
        axes[0][0].yaxis.set_ticklabels(locs)

    _set_ticks_props(axes, xlabelsize=8, xrot=90, ylabelsize=8, yrot=0)
    cdf = pd.DataFrame(cl, columns =  ["steroid_a", "steroid_b", "pearsonCorr", "pValue"])
    return axes, cdf, cm, pm


# ## Data import

# In[4]:

file = "output/01_steroids_cleaned.pickle"
steroids = pickle.load(open(file, "rb"))


# In[5]:

steroids.columns


# ## Correlation Matrix

# In[32]:

scatter_matrix(steroids, alpha=0.2, figsize=(50, 50), diagonal='kde')
plt.xticks(rotation=90)
_ = pl.savefig("img/03_steroids_correlation_matrix.png", bbox_inches = 'tight', dpi=300)


# ## Correlation Matrix Coloration

# ### Coloration exemple for two steroids

# In[7]:

pCorr, pVal = pearsonr(steroids["cr_b_cortol_d"], steroids["cr_b_cortolone_d"])
print "Pearson coef: "+str(pCorr)
print "P-Value: "+str(pVal)
axes = steroids.plot(kind='scatter', x="cr_b_cortol_d", y="cr_b_cortolone_d", color='red');
axes.patch.set_alpha(0.4)

if pVal <= 0.05:
    axes.set_axis_bgcolor('green')
else:
    axes.set_axis_bgcolor('red')

pCorr, pVal = pearsonr(steroids["cr_l7b_estradiol_d"], steroids["cr_b_cortolone_d"])
print "Pearson coef: "+str(pCorr)
print "P-Value: "+str(pVal)
axes = steroids.plot(kind='scatter', x="cr_l7b_estradiol_d", y="cr_b_cortolone_d", color='red');
axes.patch.set_alpha(0.4)
    
if pVal <= 0.05:
    axes.set_axis_bgcolor('green')
else:
    axes.set_axis_bgcolor('red')


# ### Colored Correlation Matrix 
# 
# The correlation matrix is colored according to the p-value. The p-value coloration treshold can be set with the p_min argument.

# In[6]:

# Minimal P-value set with Bonferroni correction
p_min = 0.05/((steroids.shape[1]**2-steroids.shape[1])/2)


# In[7]:

axes, cdf, cm, pm = custom_scatter_matrix(steroids, alpha=0.2, figsize=(50, 50), diagonal='kde', p_min=p_min);
_ = pl.savefig("img/03_colored_matrix.png", bbox_inches = 'tight', dpi=300)


# ## Best correlations

# In[8]:

before = cdf.shape[0]
cdf = cdf.drop_duplicates(["pearsonCorr","pValue"])
after = cdf.shape[0]
if(before != after):
    assert before == 2*after
    cdf = cdf.sort_values("pValue", axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
    cdf = cdf.reset_index(drop=True)


# ### Histograms with significative p-value
# 
# Select the significative correlations

# In[9]:

pdf = cdf[cdf.pValue < p_min]


# #### P-Value

# In[10]:

pdf["pValue"].plot.hist()


# #### Pearson

# In[11]:

pdf["pearsonCorr"].plot.hist()


# In[12]:

top = pdf.sort_values("pValue", axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
save_latex_table(top, "03_correlationTable")
save_latex_table(top.head(10), "03_top10")
top.head(10)


# ## Pearson coefficient Matrix

# In[13]:

plt.figure()
ax = plt.gca()
im = ax.matshow(cm)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
plt.colorbar(im, cax=cax)
_ = plt.savefig("img/03_pearson_matrix.png", bbox_inches = 'tight', dpi=300)


# ## P-Value Matrix

# In[14]:

plt.figure()
ax = plt.gca()
im = ax.matshow(pm)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
plt.colorbar(im, cax=cax)
_ = pl.savefig("img/03_pvalue_matrix.png", bbox_inches = 'tight', dpi=300)


# In[ ]:



