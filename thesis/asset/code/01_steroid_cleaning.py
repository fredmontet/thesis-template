
# coding: utf-8

# # 01 - Steroid Cleaning

# In[26]:

import os
import pandas as pd
import numpy as np
import skimage
import skimage.io
import pylab as pl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
get_ipython().magic(u'matplotlib inline')
import math
import pickle
from random import shuffle
from IPython.display import display, display_markdown


# ## Import the data

# In[27]:

file = "data/data_heig.dta"

# load the Stata file as a Panda DataFrame
data = pd.read_stata(file)


# In[28]:

data = data.iloc[:1129]


# In[29]:

variables = [
    'cr_andro_d', 'cr_etio_d', 'cr_saad3a17b_d', 'cr_ll_oxo_etio_d', 'cr_llb_oh_andro_d', 'cr_llb_oh_etio_d', 'cr_dha_d',
    'cr_s_ad_17b_d', 'cr_l6a_oh_dha_d', 'cr_s_at_d', 'cr_s_pt_d', 'cr_testosterone_d', 'cr_sa_dihydrotest_d',
    'cr_estriol_d', 'cr_l7b_estradiol_d', 'cr_pd_d', 'cr_pt_d', 'cr_pt_one_d',
    'cr_ths_d', 'cr_thaldo_d', 'cr_thdoc_d', 'cr_tha_d', 'cr_thb_d', 'cr_sa_thb_d', 'cr_l8_oh_tha_d',
    'cr_cortisone_d', 'cr_the_d', 'cr_a_cortolone_d', 'cr_b_cortolone_d', 'cr_zoa_dhe_d', 'cr_zob_dhe_d',
    'cr_cortisol_d', 'cr_thf_d', 'cr_sa_thf_d', 'cr_a_cortol_d', 'cr_b_cortol_d', 'cr_zoa_dhf_d', 'cr_zob_dhf_d',
    'cr_sb_oh_f_d', 'cr_l8_oh_f_d'
]
variables = [v.lower() for v in variables]
print len(variables)


# ## Missing values

# In[30]:

steroids = data[variables]
missings = steroids.isnull().as_matrix()


# In[31]:

steroids


# In[32]:

missings


# ## Steroids

# In[33]:

variables = np.sum(missings, axis=0)
plt.hist(variables, bins=len(variables))
plt.title("Missing value distribution per steroid")
plt.xlabel("Number of missing values")
plt.ylabel("Number of steroids")
plt.grid()
plt.show
_ = pl.savefig("img/01_hist_missing_steroids.png", bbox_inches = 'tight', dpi=300)
print variables


# L'histogramme ci-dessus représente le répartition des valeurs manquantes des stéroides. On constate que la plupart ont moins de 100 valeurs manquantes. On va enlever les stéroides qui comportent plus de 100 valeurs manquantes.

# ## Patients

# In[34]:

patients = np.sum(missings, axis=1)
plt.hist(patients, bins=40)
plt.title("Missing values distribution per patient")
plt.xlabel("Number of missing values")
plt.ylabel("Number of patient")
plt.grid()
plt.show
_ = pl.savefig("img/01_hist_missing_patients.png", bbox_inches = 'tight', dpi=300)


# L'histogramme ci-dessus représente le répartition des valeurs manquantes des patients. La plupart ont moins de 5 valeurs manquantes. On enlevera les patients avec plus de 5 valeurs manquantes.

# ## Drop rows and columns with too little values

# In[35]:

colmask = np.sum(missings, axis=0) > 100
print "columns to remove : ", steroids.columns[np.flatnonzero(colmask)]

rowmask = np.sum(missings, axis=1) > 5
print "patients to remove : ", np.flatnonzero(rowmask)

print "shape before missing removal", steroids.shape
valid_rows = np.flatnonzero(~rowmask)
steroids = steroids.iloc[valid_rows]
steroids = steroids[steroids.columns[~colmask]]

print "shape after missing removal", steroids.shape
steroids = steroids.fillna(steroids.mean())

# Z-Score noramlisation
steroids = (steroids - steroids.mean()) / (steroids.std())


# ## Apply the same patient removal to the patients' hash_id

# In[21]:

hash_id = pickle.load(open("output/00_hash_id.pickle", "rb"))
hash_id = hash_id.iloc[valid_rows]
hash_id.shape


# ## Per-variable boxplot

# In[11]:

plt.figure(figsize=(20, 10))
plt.ylim((-4, 6))
steroids.boxplot();
plt.xticks(rotation=90)
_ = pl.savefig("img/01_boxplots_clean_steroids.png", bbox_inches = 'tight', dpi=300)


# ## Save cleaned dataset 

# In[41]:

steroids.to_pickle('output/01_steroids_cleaned.pickle')
hash_id.to_pickle('output/01_hash_id_cleaned.pickle')

filename = "output/01_patients_to_keep.pickle"  
with open(filename, "w+b") as f:
    pickle.dump(valid_rows, f)
    
filename = "output/01_steroids_index_cleaned.pickle"  
with open(filename, "w+b") as f:
    pickle.dump(np.array(steroids.index), f)

