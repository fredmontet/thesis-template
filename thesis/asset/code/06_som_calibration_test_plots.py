
# coding: utf-8

# # 06 - SOM Calibration test - Plots

# In[3]:

import os
import math
import pickle
import pandas as pd
import numpy as np
from som import som
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
get_ipython().magic(u'matplotlib inline')
from IPython.display import display, display_markdown
from random import shuffle


# # Data import

# In[4]:

# Load the data
results = pickle.load(open('output/05_results_animals_100_100.pickle', "rb"))
summaries = pickle.load(open('output/05_summaries_animals_100_100.pickle', "rb"))


# In[5]:

# Set the first SOM size summary as exemple
initial = summaries[10][10]


# ## Plot de l'erreur médiane

# Erreur médiane sur les n exécutions faites durant le grid run

# In[6]:

fig, ax = plt.subplots(figsize = (10,5))
plt.title("Median error")
plt.xlabel('Iterations')
plt.ylabel('Error')
ax = plt.plot(initial["median_error"], '-r')
ax = plt.fill_between(initial.index, initial["lower_whisker_error"], initial["upper_whisker_error"], facecolor='red', alpha=0.3)
_ = plt.savefig("img/06_median_error_test.png", bbox_inches = 'tight', dpi=300)


# ## Vue globale de l'erreur médiane

# In[7]:

fig2 = plt.figure(figsize=(40,40))

h = len(results.index)
w = len(results[results.index.tolist()[0]].index)
h0 = results.index.tolist()[0]
w0 = results[results.index.tolist()[0]].index.tolist()[0]
hGap = results.index.tolist()[1]-h0
wGap = results[results.index.tolist()[0]].index.tolist()[1]-w0
gs = gridspec.GridSpec(h, w)

for i in range(h):
    for j in range(w):
        ax = plt.subplot(gs[(i*h)+j])
        xdim = (i+1)*hGap
        n_iter = (j+1)*wGap
        plt.title("xdim:"+str(xdim)+" n_iter:"+str(n_iter))  
        plt.xlabel('Iterations')
        ax.set_xlim([0, results.index.tolist()[-1]])
        ax.set_ylim([0, np.around(summaries[h0][w0]["median_error"].max(),1)])
        plt.ylabel('Error')
        plt.plot(summaries[xdim][n_iter]["median_error"], '-r')

plt.tight_layout(pad=0, w_pad=0.5, h_pad=1.2)
_ = plt.savefig("img/06_global_median_error_test.png", bbox_inches = 'tight', dpi=300)


# ## Dérivée de l'erreur médiane

# ### Plot de la dérivée de l'erreur et de l'erreur

# In[8]:

plt.figure(figsize=(10, 5))
plt.plot(initial["median_error"], 'b--')
plt.plot(initial["slope"], 'r')
_ = plt.savefig("img/06_error_derivative_test.png", bbox_inches = 'tight', dpi=300)


# ### Plot de la dérivée de l'erreur

# In[9]:

plt.figure(figsize=(10, 5))
plt.plot(initial["slope"], 'r')
_ = plt.savefig("img/06_derivative_test.png", bbox_inches = 'tight', dpi=300)


# # Optimisation avec la dérivée

# In[10]:

seuil = 0.5 # Valeur minimale de la dérivée pour déterminer la convergence
optimisations = pd.DataFrame()

for size in summaries:
    iterations = []
    for run in summaries[size]:
        it = 0
        for idx, val in enumerate(run["slope"]):
            if abs(val) < seuil:
                if it == 3:
                    line = run.loc[run["slope"] == val]
                    iterations.append(line)
                    break
                it += 1
            elif abs(val) >= seuil:
                iterations.append(np.nan)
                break
    optimisations[size] = iterations
optimisations.index = results.index


# In[11]:

# X = dimension (xdim)
# Y = Nb of iterations (n_iter)
optimisations


# In[12]:

optimisations[30][40]


# ## Plot 1: Nombre d'itérations pour convergence
# 
# - En X: les dimensions de la carte (10x10, 20x20, ..., NxN)
# - En Y: les itérations nécessaire
# 
# Le graphe représentera donc le nombre d'itérations nécessaire pour converger, pour une dimension de carte donnée.

# In[13]:

seuil = 0.05 # Valeur minimale de la dérivée pour déterminer la convergence

optimisations1 = pd.DataFrame()

for size in summaries:
    iterations = []
    for run in summaries[size]:
        it = 0
        for idx, val in enumerate(run["slope"]):
            if abs(val) < seuil:
                if it == 3:
                    line = run.loc[run["slope"] == val]
                    iterations.append(line.index.values[0])
                    break
                it += 1
            elif abs(val) >= seuil:
                iterations.append(np.nan)
                break
    optimisations1[size] = iterations
optimisations1.index = results.index


# ### Vue d'ensemble

# In[14]:

fig = plt.figure()
ax = fig.gca(projection='3d')

X = optimisations.index
Y = optimisations[10].index

X, Y = np.meshgrid(X, Y)
Z = optimisations1

surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet,
                       linewidth=0, antialiased=True)

ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)
plt.tight_layout()
plt.show()


# In[15]:

"""
fig = plt.figure(figsize = (10,5))
plt.title("Needed iterations for convergence")
plt.xlabel('Map size')
plt.ylabel('Convergence at iteration')
plt.grid()
plt.plot(optimisation.index, optimisation["convergence"])
"""


# ## Plot 2: Erreur minimum en fonction de la taille

# In[16]:

seuil = 0.5 # Valeur minimale de la dérivée pour déterminer la convergence

optimisations2 = pd.DataFrame()

for size in summaries:
    iterations = []
    for run in summaries[size]:
        it = 0
        for idx, val in enumerate(run["slope"]):
            if abs(val) < seuil:
                if it == 3:
                    line = run.loc[run["slope"] == val]
                    iterations.append(line["median_error"].values[0])
                    break
                it += 1
            elif abs(val) >= seuil:
                iterations.append(np.nan)
                break
    optimisations2[size] = iterations
optimisations2.index = results.index


# ### Vue d'ensemble

# In[17]:

fig = plt.figure()
ax = fig.gca(projection='3d')

X = optimisations.index
Y = optimisations[10].index

X, Y = np.meshgrid(X, Y)
Z = optimisations2

surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet,
                       linewidth=0, antialiased=True)

ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
plt.ylabel("xdim")
plt.xlabel("n_iter")
for angle in range(0, 360):
    ax.view_init(30, -60)

fig.colorbar(surf, shrink=0.5, aspect=5)
plt.tight_layout()

plt.show()

_ = pl.savefig("img/06_minimum_error_test.png", bbox_inches = 'tight', dpi=300)


# ### Vue de détail

# In[ ]:

"""
fig = plt.figure(figsize = (10,5))
plt.title("Minimal error at convergence")
plt.xlabel('Iterations')
plt.ylabel('Error')
plt.grid()
plt.plot(optimisation["size"], optimisation["value"])
"""


# # Optimisation avec l'erreur

# In[ ]:

seuil = 0.5 # Valeur minimale de la dérivée pour déterminer la convergence

optimisations3 = pd.DataFrame()

for size in summaries:
    iterations = []
    for run in summaries[size]:
        iterations.append(run["median_error"].iloc[run.index[-1]])
    optimisations3[size] = iterations
optimisations3.index = results.index


# In[ ]:

optimisations3.round(2)


# In[ ]:

fig = plt.figure()
ax = fig.gca(projection='3d')

X = optimisations.index
Y = optimisations[10].index

X, Y = np.meshgrid(X, Y)
Z = optimisations3

surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet,
                       linewidth=0, antialiased=True)

ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
# X = dimension (xdim)
# Y = Nb of iterations (n_iter)
fig.colorbar(surf, shrink=0.5, aspect=5)
ax.set_xlabel('xdim')
ax.set_ylabel('n_iter')
ax.set_zlabel('error')

#for angle in range(0, 360):
ax.view_init(30, 60)

plt.tight_layout()
_ = plt.savefig("img/06_grid_error_test.png", bbox_inches = 'tight', dpi=300)
plt.show()

