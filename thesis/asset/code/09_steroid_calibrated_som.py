
# coding: utf-8

# # 09 - Steroid calibrated SOM

# In[4]:

import os
import math
import pickle
import pandas as pd
import numpy as np
from som import som
import scipy.constants as const
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
get_ipython().magic(u'matplotlib inline')
from IPython.display import display, display_markdown
from random import shuffle


# ## Data import

# In[5]:

steroids = pickle.load(open("output/01_steroids_cleaned.pickle", "rb"))

attributes = [
        'cr_saad3a17b_d', 'cr_ll_oxo_etio_d', 'cr_llb_oh_andro_d', 'cr_llb_oh_etio_d', 'cr_dha_d',
        'cr_s_ad_17b_d', 'cr_l6a_oh_dha_d', 'cr_s_at_d', 'cr_s_pt_d', 'cr_testosterone_d', 'cr_sa_dihydrotest_d',
        'cr_estriol_d', 'cr_l7b_estradiol_d', 'cr_pd_d', 'cr_pt_d', 'cr_pt_one_d',
        'cr_ths_d', 'cr_thaldo_d', 'cr_thdoc_d', 'cr_tha_d', 'cr_thb_d', 'cr_sa_thb_d', 'cr_l8_oh_tha_d',
        'cr_cortisone_d', 'cr_the_d', 'cr_a_cortolone_d', 'cr_b_cortolone_d', 'cr_zoa_dhe_d', 'cr_zob_dhe_d',
        'cr_cortisol_d', 'cr_a_cortol_d', 'cr_b_cortol_d', 'cr_zoa_dhf_d', 'cr_sb_oh_f_d', 'cr_l8_oh_f_d'
]

attributes = [v.lower() for v in attributes]
print len(attributes)


# In[6]:

matrix_steroids = steroids.as_matrix()


# ## Calcul d'une SOM

# In[11]:

params = {
            'n_iter' : 30, 
            'x' : 40,
            'y' : int(np.ceil(const.golden*40)),
            'dimension' : len(attributes),
            'learning_rate' : (0.012, 0.001),
            'neighborhood_size' : (int(np.ceil(1./2*int(np.ceil(const.golden*40)))), 1) 
            #'neighborhood_size' : (int(np.ceil(0.7/10*int(np.ceil(const.golden*30)))), 1) 
        }


# In[9]:

somSteroids = som.SOM(matrix_steroids, attributes, params)


# ## U-Matrix

# In[12]:

somSteroids.getUMatrix()
_ = plt.savefig("img/09_steroid_calibrated_som.png", bbox_inches = 'tight', dpi=300)


# ## Component planes

# In[13]:

somSteroids.getComponentPlanes()
_ = plt.savefig("img/09_steroid_calibrated_component_planes.png", bbox_inches = 'tight', dpi=300)


# ## Plot de l'erreur

# In[14]:

somSteroids.getErrorPlot()
_ = plt.savefig("img/09_steroid_calibrated_error_plot.png", bbox_inches = 'tight', dpi=300)


# ## Save the calibrated SOM

# In[15]:

# Save the SOM
filename = "output/09_steroid_calibrated_som.pickle"  
with open(filename, "w+b") as f:
    pickle.dump(somSteroids, f)

